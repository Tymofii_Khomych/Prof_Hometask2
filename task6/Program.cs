﻿namespace task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SortedList<string, int> sortedList = new SortedList<string, int>();

            sortedList.Add("Apple", 3);
            sortedList.Add("Banana", 2);
            sortedList.Add("Cherry", 5);
            sortedList.Add("Durian", 4);
            sortedList.Add("Elderberry", 1);

            Console.WriteLine("Відсортований порядок:");

            // Вивести пари "ключ-значення" в алфавітному порядку
            foreach (var pair in sortedList)
            {
                Console.WriteLine($"Ключ: {pair.Key}, Значення: {pair.Value}");
            }

            Console.WriteLine();

            Console.WriteLine("Зворотний порядок:");

            // Вивести пари "ключ-значення" у зворотному порядку
            for (int i = sortedList.Count - 1; i >= 0; i--)
            {
                var pair = sortedList.ElementAt(i);
                Console.WriteLine($"Ключ: {pair.Key}, Значення: {pair.Value}");
            }
        }
    }
}