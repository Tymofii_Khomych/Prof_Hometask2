﻿namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1.
            //List<Tuple<int, decimal>> accounts = new List<Tuple<int, decimal>>();
            //accounts.Add(new Tuple<int, decimal>(1, 100.50m));
            //accounts.Add(new Tuple<int, decimal>(2, 200.75m));

            //accounts.Add(new Tuple<int, decimal>(3, 300.90m));

            //decimal availableAmount = accounts.Find(x => x.Item1 == 2)?.Item2 ?? 0;
            //Console.WriteLine(availableAmount);

            // 2.
            //Dictionary<int, decimal> accounts = new Dictionary<int, decimal>();
            //accounts.Add(1, 100.50m);
            //accounts.Add(2, 200.75m);

            //accounts.Add(3, 300.90m);

            //decimal availableAmount = accounts.ContainsKey(2) ? accounts[2] : 0;
            //Console.WriteLine(availableAmount);

            // 3.
            List<Account> accounts = new List<Account>();
            accounts.Add(new Account { Number = 1, AvailableAmount = 100.50m });
            accounts.Add(new Account { Number = 2, AvailableAmount = 200.75m });

            accounts.Add(new Account { Number = 3, AvailableAmount = 300.90m });

            decimal availableAmount = accounts.Find(x => x.Number == 2)?.AvailableAmount ?? 0;
            Console.WriteLine(availableAmount);
        }
    }
}