﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Account
    {
        public int Number { get; set; }
        public decimal AvailableAmount { get; set; }
    }
}
