﻿using System.Text;

namespace task2
{
    internal class Program
    {
        static void AddPurchase(Dictionary<string, List<string>> purchases, string buyer, string category)
        {
            if (!purchases.ContainsKey(buyer))
            {
                purchases[buyer] = new List<string>();
            }
            purchases[buyer].Add(category);
        }

        static List<string> GetCategoriesByBuyer(Dictionary<string, List<string>> purchases, string buyer)
        {
            if (purchases.ContainsKey(buyer))
            {
                return purchases[buyer];
            }
            return new List<string>();
        }

        static List<string> GetBuyersByCategory(Dictionary<string, List<string>> purchases, string category)
        {
            List<string> buyers = new List<string>();
            foreach (var kvp in purchases)
            {
                if (kvp.Value.Contains(category))
                {
                    buyers.Add(kvp.Key);
                }
            }
            return buyers;
        }

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            Dictionary<string, List<string>> purchases = new Dictionary<string, List<string>>();

            AddPurchase(purchases, "Покупець 1", "Категорія 1");
            AddPurchase(purchases, "Покупець 2", "Категорія 1");
            AddPurchase(purchases, "Покупець 1", "Категорія 2");
            AddPurchase(purchases, "Покупець 3", "Категорія 3");

            List<string> categoriesOfBuyer1 = GetCategoriesByBuyer(purchases, "Покупець 1");
            Console.WriteLine("Категорії товарів, куплених Покупцем 1:");
            foreach (string category in categoriesOfBuyer1)
            {
                Console.WriteLine(category);
            }

            List<string> buyersOfCategory1 = GetBuyersByCategory(purchases, "Категорія 1");
            Console.WriteLine("Покупці, які купили товари з Категорії 1:");
            foreach (string buyer in buyersOfCategory1)
            {
                Console.WriteLine(buyer);
            }
        }
    }
}