﻿namespace task4
{
    internal class Program
    {

        static void Main(string[] args)
        {
            OrderedDictionary<string, string> dictionary = new OrderedDictionary<string, string>();
            dictionary.Add("One", "1");
            dictionary.Add("Two", "2");
            dictionary.Add("Three", "3");

            Console.WriteLine(dictionary["1"]); // Виведе: 1
            Console.WriteLine(dictionary["2"]); // Виведе: 3

            Console.WriteLine(dictionary.ContainsKey("1")); // Виведе: True
            Console.WriteLine(dictionary.ContainsKey("4")); // Виведе: False
        }
    }
}