﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    public class OrderedDictionary<TKey, TValue> : IEnumerable<TValue>
    {
        private readonly List<TValue> items;
        private readonly Dictionary<TKey, TValue> keyLookup;

        public OrderedDictionary()
        {
            items = new List<TValue>();
            keyLookup = new Dictionary<TKey, TValue>();
        }

        public int Count => items.Count;

        public void Add(TValue item, TKey key)
        {
            items.Add(item);
            keyLookup.Add(key, item);
        }

        public bool ContainsKey(TKey key)
        {
            return keyLookup.ContainsKey(key);
        }

        public TValue this[TKey key]
        {
            get
            {
                if (keyLookup.TryGetValue(key, out var value))
                {
                    return value;
                }

                throw new KeyNotFoundException($"Key '{key}' not found in the OrderedDictionary.");
            }
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}
